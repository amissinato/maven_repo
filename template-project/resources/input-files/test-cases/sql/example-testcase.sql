CREATE TABLE <mytable>(
   color       VARCHAR(6) NOT NULL PRIMARY KEY
  ,category    VARCHAR(5) NOT NULL
  ,type        VARCHAR(9)
  ,codergba0   INTEGER  NOT NULL
  ,codergba1   INTEGER  NOT NULL
  ,codergba2   INTEGER  NOT NULL
  ,codergba3   BIT  NOT NULL
  ,codehex     VARCHAR(4) NOT NULL
);
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('black','hue','primary',255,255,255,1,'#000');
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('white','value',NULL,0,0,0,1,'#FFF');
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('red','hue','primary',255,0,0,1,'#FF0');
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('blue','hue','primary',0,0,255,1,'#00F');
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('yellow','hue','primary',255,255,0,1,'#FF0');
INSERT INTO <mytable>(color,category,type,codergba0,codergba1,codergba2,codergba3,codehex) VALUES ('green','hue','secondary',0,255,0,1,'#0F0');
